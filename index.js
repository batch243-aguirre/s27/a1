// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}
{
    "id": 2,
    "firstName": "Michael",
    "lastName": "Aguirre",
    "email": "michaelluisaguirre022@gmail.com",
    "password": "michael123",
    "isAdmin": false,
    "mobileNo": "09348621780"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}
{
    "id": 11,
    "userId": 2,
    "productID" : 35,
    "transactionDate": "11-24-2022",
    "status": "paid",
    "total": 2500
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}
{
    "id": 35,
    "name": "Electric fan",
    "description": "A fan powered by machine used to create a flow of air.",
    "price": 1500,
    "stocks": 285,
    "isActive": true,
}

